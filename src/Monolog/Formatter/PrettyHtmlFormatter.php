<?php

namespace Drupal\monolog_file\Monolog\Formatter;

use Monolog\Formatter\HtmlFormatter;

class PrettyHtmlFormatter extends HtmlFormatter
{

  const HEADER_LENGTH = 100;
  const header_tag = 'h2';
  const  TBODY_HEIGHT = 150;

  /**
   * Create a HTML h1 tag
   *
   * @param  string $title Text to be in the h1
   * @param  int    $level Error level
   * @return string
   */
  protected function addTitle($title, $level)
  {
    $title = htmlspecialchars($title, ENT_NOQUOTES, 'UTF-8');

    return '<'.SELF::header_tag.' style="background: '.$this->logLevels[$level]
      .';color: #ffffff;padding: 5px;" class="monolog-output">'
      .$title.'</'.SELF::header_tag.'>';
  }

  /**
   * Formats a log record.
   *
   * @param array $record A record to format
   * @return mixed The formatted record
   */
  public function format(array $record)
  {
    $message = (string)$record['message'];
    $length = strpos($message, 'php');
    if ($length < SELF::HEADER_LENGTH) {
      $length = SELF::HEADER_LENGTH;
    }
    $title = substr($message, 0, $length);
    $output = $this->addTitle($title, $record['level']);
    $output .= '<table style="display: block;  cellspacing="1" width="100%" class="monolog-output">
                <tbody style="display: block; height: '.SELF::TBODY_HEIGHT.'px; overflow: scroll;">';

    $output .= $this->addRow('Message', $message);
    $output .= $this->addRow('Time', $record['datetime']->format($this->dateFormat));
    $output .= $this->addRow('Channel', $record['channel']);
    if ($record['context']) {
      $embeddedTable = '<table cellspacing="1" width="100%">
                        <tbody style="display: block; height: '.SELF::TBODY_HEIGHT.'px; overflow: scroll;">';

      foreach ($record['context'] as $key => $value) {
        $embeddedTable .= $this->addRow($key, $this->convertToString($value));
      }
      $embeddedTable .= '</tbody></table>';
      $output .= $this->addRow('Context', $embeddedTable, false);
    }
    if ($record['extra']) {
      $embeddedTable = '<table cellspacing="1" width="100%">
                        <tbody style="display: block; height: '.SELF::TBODY_HEIGHT.'px; overflow: scroll;">';

      foreach ($record['extra'] as $key => $value) {
        $embeddedTable .= $this->addRow($key, $this->convertToString($value));
      }
      $embeddedTable .= '</tbody></table>';
      $output .= $this->addRow('Extra', $embeddedTable, false);
    }

    return $output . '</tbody></table>';
  }
}
