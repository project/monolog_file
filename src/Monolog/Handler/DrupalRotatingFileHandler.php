<?php

/*
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drupal\monolog_file\Monolog\Handler;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

/**
 * Stores logs to files that are rotated every day and a limited number of files are kept.
 *
 * This rotation is only intended to be used as a workaround. Using logrotate to
 * handle the rotation is strongly encouraged when you can use it.
 */
class DrupalRotatingFileHandler extends RotatingFileHandler
{
  protected function getTimedFilename()
  {
    $private_path = \Drupal::service('stream_wrapper_manager')->getViaUri('private://');
    $this->filename = $private_path->getDirectoryPath().$this->filename;

    $fileInfo = pathinfo($this->filename);
    $timedFilename = str_replace(
      array('{filename}', '{date}'),
      array($fileInfo['filename'], date($this->dateFormat)),
      $fileInfo['dirname'] . '/' . $this->filenameFormat
    );

    if (!empty($fileInfo['extension'])) {
      $timedFilename .= '.'.$fileInfo['extension'];
    }
    return $timedFilename;
  }
}
