<?php

namespace Drupal\monolog_file\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * Class LogFileController.
 */
class LogFileController extends ControllerBase {

  function __construct()
  {
    $this->logDirPath = \Drupal::service('file_system')->realpath("private://").DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR;
  }

  /**
   * Hello.
   *
   * @return array
   *   Return log files list.
   */
  public function listFiles() {
    return [
      '#theme' => 'item_list',
      '#items' => $this->getLogFilesList(),
      '#title' => $this->t('log files:'),
      '#list_type' => 'ul',
      '#attributes' => ['class' => 'mylist'],
      '#wrapper_attributes' => ['class' => 'container'],
    ];
  }

  public function getLogFilesList(){
    $linked_files = [];

    $files = glob($this->logDirPath.'*.log');
    foreach ($files as $file) {
      $fileList[filemtime($file)] = $file;
    }
    ksort($fileList);
    $fileList = array_reverse($fileList, TRUE);
    foreach($fileList as $k => $file){
      $fileinfo = pathinfo($file);
      $linked_files []= [$this->getFileLink($fileinfo['filename'], $fileinfo['basename'])];
    }
    return $linked_files;
  }

  public function getFileLink($name, $filename){
    return Link::createFromRoute(
      $name,
      'monolog_file.read_log_file',
      ['fileName' => $this->encodeFileName($filename)],
      ['attributes' => ['target' => '_blank']]
    )
      ->toRenderable();
  }

  public function encodeFileName($filename){
    return base64_encode($filename);
  }

  public function decodeFileName($encodedFileName){
    return base64_decode($encodedFileName);
  }

  public function readFile(){
    $encodedFileName = \Drupal::routeMatch()->getParameter('fileName');
    $fileName = $this->decodeFileName($encodedFileName);
    $filePath = $this->logDirPath.$fileName;

    header("Content-type: text/html");
    header('Cache-Control: no-cache, must-revalidate');
    header("Content-length:".(string)(filesize($filePath)));
    header('Pragma: no-cache');
    $fp = fopen($filePath, 'rb');

    //scroll to bottom to see recent log.
echo "<script type='text/javascript'>
var interval = setInterval(function() {
var scrollingElement = (document.scrollingElement || document.body);
scrollingElement.scrollTop = scrollingElement.scrollHeight;
clearInterval(interval);
}, 100);
</script>";
    @fpassthru($fp);
    ob_start();
    ob_end_clean();
    exit;
  }
}
