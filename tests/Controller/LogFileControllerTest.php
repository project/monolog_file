<?php

namespace Drupal\monolog_file\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the monolog_file module.
 */
class LogFileControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "monolog_file LogFileController's controller functionality",
      'description' => 'Test Unit for module monolog_file and controller LogFileController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests monolog_file functionality.
   */
  public function testLogFileController() {
    // Check that the basic functions of module monolog_file.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
