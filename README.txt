-- SUMMARY --

Monolog file

This module provides new log handler to save logs in private files by extending the rotating file handler.

-- REQUIREMENTS --

The monolog module.

Configuration
  See monolog_file.services.yml

Usage
  Download and enable monolog by composer require project/monolog
  Enable the monolog_file module
  Access log files form /admin/reports/log_files
